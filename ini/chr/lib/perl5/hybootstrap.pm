#***keyword-flag***     "Version %v  %f"
# "Version 14  10-Apr-12,16:19:56"

use strict;
require 'hydata.pl';
package HyBootstrap;

my $div_count = 0;
=documentation 

CSS from Twitter Bootstrap Requires:
..\ini\css\bootstrap.css

Use like HYTABLE.
  $bootdiv->Div('opening tag','Text','closing tag');
 E.G.
  $bootdiv->Div('h1','Legacy Logger Report','h1');
 
If you don't want to close the tag, don't enter the closing tag. Then close it later.
Use Glyphicons with the 'i' tag. See http://twitter.github.io/bootstrap/ for more details.
 E.G. 
  $bootdiv->Div('th','','');
  $bootdiv->Div('i class="icon-info-sign"','','i');
  $bootdiv->Div('','','th');  

=cut

my ($prt_trace)='-T';
my $_debug=defined($ENV{'HYDEBUG'});

#-------------------------------------------------------------------------------------------------------------- New
sub New{
  # Create a new instance of this object.
  #    my($table)=HyTable->New();

  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {};
  bless ($self, $class);        #magic that I don't fully understand
  
  ::Prt($prt_trace,"Initialising instance of HYTABLE.PM\n") if $_debug;
  
  $self->Reset(@_);
  
  ::Prt($prt_trace,"Initialising complete\n") if $_debug;
  
  ::Prt("-L","----NEW prot [$proto] class [$class] self [$self]\n");
  foreach my $element (@_){
    ::Prt("-L","----NEW HashDump of \@_ [$element]\n");
  }
  
  ::Prt("-L","----NEW HashDump of proto \n[\n".ref($proto)."\n]\n");
  
  ::Prt("-L","----NEW HashDump of self \n[\n".ref($self)."\n]\n");
  return ($self);
}


sub Div{
  $div_count++;
  ::Prt("-RSX","Text parameters are Self, Class, *StringArray, Closing_class, you gave ",join("|",@_),"\n") if $#_ !=3;
  my $self          = shift;
  my $class         = shift;
  my $string        = shift;
  my $closing_class = shift;
  
  my (@strings,$result);
  ::Prt($prt_trace,"Calling Text with [$class]:[$string]\n") if $_debug;

  if (defined $class && $class ne ''){
    $result="<$class>";
  }
  
  if (defined $string && $string ne ''){
    $result.="$string";
  }
  
  if (defined $closing_class && $closing_class ne ''){
    $result.="</$closing_class>";
  }
  $self->{'div'}[$div_count] = $result;
  return $result;
}

sub Image{
  ::Prt("-RSX","Image parameters are Self, Image, Width, Height, Align, you gave ",join("|",@_),"\n") if $#_ !=4;
  my $self = shift;
  my $image= shift;
  my $width= shift;
  my $height=shift;
  my $align= shift;
  
  my ($result);
  ::Prt($prt_trace,"Calling Image with [$image] [$align] [$width] [$height]\n") if $_debug;
  $image=~s/\\/\//g; #reverse the slashes so they lean forwards
  $result="<img src='$image' ";
  $result.=($width !=0)?"width='$width' ":"";
  $result.=($height !=0)?"height='$height' ":"";
  $result.=($align ne "")?"align='$align' ":"";
  $result.=">";
  ::Prt($prt_trace,"Image returning [$result]\n") if $_debug;
  return $result;
}

sub HTML {
  #return HTML string for table
  
  ::Prt("-RSX","HTML parameters are Self, you gave ",join("|",@_),"\n") if $#_ !=0;
  my $self = shift;
   
  my ($tablewidth,$tableclass,$tableid,$tablecellspacing,$tablecellpadding,$tablepadding);
  my ($col,$row,$htm,$s,$class,$id,$colspan,$width,$align,$valign,$span,$title,$border,$href,$hrefend);
  
  ::Prt($prt_trace,"Returning HTML for table [$$self{'row_size'},$$self{'col_size'}]\n") if $_debug;
  
  my @divs = @{$self->{'div'}};
  foreach my $div ( 1..$#divs){
      $htm.= $divs[$div];
  }
  ::Prt($prt_trace,"HTML=[$htm]\n") if $_debug;
  return $htm;
}
  
sub Page {
  #return complete html page for table
  
  ::Prt("-RSX","Page parameters are Self, you gave ",join("|",@_),"\n") if $#_ !=0;
  my $self = shift;
  
  
  foreach my $dir ('js','css','img'){
    my $ini_dir = ::HyconfigValue('INIPATH')."$dir\\";
    my @files = ::DOSFileList($ini_dir);
    my $file_count = scalar @files;
    my $junk_dir = ::HyconfigValue('JUNKPATH')."$dir\\";
    ::MkDir($junk_dir);
    
    foreach my $file (@files){
      my $file_ext = ::FileNameExt($file);
      my $destfile = $junk_dir.$file_ext;
      ::copy($file,$destfile);
    }
  }
  
  my ($page,$title,$html);
  $title=$self->{'tab'}{'tabletitle'};
  $page=$self->{'template'};
  $html=$self->HTML;
  my $script = '<script>$(document).ready(function(){$("table").tablesorter();});</script>';
  #$page=~s/\[\[content\]\]/$self->HTML/e;
  $page=~s/\[\[content\]\]/$html/e;
  
  return $page;
}


sub Reset {
  my $self = shift;
  my $template = shift;
  
  $self->{'div'} = ();
  $self->{'valid'}{'div'}{'class'}=1; 
  $self->{'valid'}{'div'}{'id'}=1; 
  $self->{'valid'}{'div'}{'align'}=1;
  
  $self->{'table'} = ();
  $self->{'valid'}{'table'}{'class'}=1; 
  $self->{'valid'}{'table'}{'id'}=1; 
  $self->{'valid'}{'table'}{'align'}=1;
  
  $self->{'thead'} = ();
  $self->{'valid'}{'thead'}{'class'}=1; 
  $self->{'valid'}{'thead'}{'id'}=1; 
  $self->{'valid'}{'thead'}{'align'}=1;
  
  $self->{'tbody'} = ();
  $self->{'valid'}{'tbody'}{'class'}=1; 
  $self->{'valid'}{'tbody'}{'id'}=1; 
  $self->{'valid'}{'tbody'}{'align'}=1;

  $self->{'th'} = ();
  $self->{'td'} = ();
  $self->{'tr'} = ();
  $self->{'valid'}{'tr'}{'class'}=1; 
  
  $self->{'caption'} = ();
  $self->{'valid'}{'caption'}{'class'}=1; 
  $self->{'valid'}{'caption'}{'id'}=1; 
  $self->{'valid'}{'caption'}{'align'}=1;
  
  $self->{'abbr'} = ();
  $self->{'valid'}{'abbr'}{'class'}=1; 
  $self->{'valid'}{'abbr'}{'id'}=1; 
  $self->{'valid'}{'abbr'}{'align'}=1;
   
  #read the template
  if($template eq ''){$template='hytable'};
  if($template !~/\.htm$/){$template.='.htm'};
  my $path;
 
 foreach $path ('temppath','inipath','miscpath'){
    if(-f ::HyconfigValue($path).$template){
      ::Prt($prt_trace,"Using template from ",::HyconfigValue($path),".$template\n") if $_debug;
      ::OpenFile(*TEMPLATE,::HyconfigValue($path).$template,"<");
      undef $/;
      $self->{'template'}=<TEMPLATE>;
      close TEMPLATE;
      $/="\n";
      last;
    }
  }
  if($self->{'template'} eq ''){::Prt("-RSX","Could not find template $template\n")};
  
  ::Prt($prt_trace,"Setting properties in Reset $_[0]\n") if $_debug;
    
}

  
1;  # so the require or use succeeds
