
{
package Program;
#use lib "$ENV{INIPATH}Chromicon\\pagages"; 
use Moose;


has 'company' => (
	is  => 'ro',
	isa => 'Str',
	);
  
has 'program' => (
	is  => 'ro',
	isa => 'Str',
	);
  
has 'extensions' => (
	is  => 'ro',
	isa => 'Str',
	);

  
  
  sub Initialize2 {
  
  #sub speak { $_[0]->sound }
    
  }
}






{
package Chromicon;
#use lib "$ENV{INIPATH}chr\\lib\\Hydstra";
use Moose;
use Moose::Util::TypeConstraints;
#use MooseX::Constructor::AllErrors;

=skip
require Hydstra::Tables;
#require Hydstra::Fields;

has 'table' => (
	is  => 'rw',
	isa => 'ArrayRef[Tables]',
  auto_deref => 1,
	);
=cut
#has 'field' => (
#	is  => 'rw',
#	isa => 'ArrayRef[Fields]',
#  auto_deref => 1,
#	);
  
  __PACKAGE__->meta->error_class('Moose::Error::Croak');
 no Moose; 
 
}


{
package Chromicon::HyData;
use 5.010;
use Moose;
use HydDllp;


#use Data::Dumper;
my $DEFBUFFER        = 1950;





=skip
  sub create_history{
    #$chr->create_history($station,$statdate,$stattime,$keyword,$descript);
    my $c = shift;
    my $station = shift;
    my $statdate = shift;
    my $stattime = shift;
    my $keyword = shift;
    my $descript = shift;

    use HyDB;
    require 'hydlib';
    
    my $repfile = JunkFile('txt');
    my $table = 'HISTORY';
    my $workarea = "[PUBLIC.$table]";
    my %history;
    
    #Create printed History record
    $history{STATION} = uc($station);
    $history{STATDATE} = $statdate;
    $history{STATTIME} = $stattime;
    $history{KEYWORD} = $keyword;
    $history{DESCRIPT} = $descript;
      
    #Write exports to workarea
    my $db_handle = HyDB->new( $table, $workarea, { allowdupes => 0, printdest => *hREPORT, errordest => *hREPORT } );
    $db_handle->sethash( \%history );
    $db_handle->write();
    $db_handle->clear();
    $db_handle->close();
        
    PrintAndRun( '-L', qq(HYDBUTIL APPEND $table [$workarea]$table TABLE NO "$repfile+" /FASTMODE) ); 
      
    return;
  }


=cut






  sub site_group{
    my $c = shift;
    my $group = shift;
    my $sitelist_filter = "GROUP($group)";
    
    my $dllp = HydDllp->New();
    my %d = $dllp->get_db_info(
      { 'function' => 'get_db_info',
        'version'  => 3,
        'params'   => {
          'table_name'  => 'site',
          'return_type' => 'hash',
          'sitelist_filter' => $sitelist_filter
        }
      },
      $DEFBUFFER
    );
    $dllp->Close();
    #Prt( '-P', NowStr() . "   - return\n".HashDump(\%hydtables)."]\n" );
    
    #print Dumper(\%d);
    return %d;
  }
  
  
  sub get_hydb_info{
    my $c = shift;
    my $table = shift;
    my $site = shift ;
    my $fields = shift ;
    
    my %params;
    
    if ( !defined $fields && !defined $site && defined $table){
      $params{table_name} = $table;
      $params{return_type} = 'hash';
    }      
    elsif( !defined $fields && defined $site && defined $table ){
      $params{table_name} = $table;
      $params{return_type} = 'hash';
      $params{sitelist_filter} = $site;
    }
    elsif( defined $fields && defined $site && defined $table ){
      my @field_list = @$fields;
      $params{table_name} = $table;
      $params{return_type} = 'hash';
      $params{sitelist_filter} = $site;
      $params{field_list} = \@field_list;
    }

    my $dllp = HydDllp->New();
    my %d = $dllp->get_db_info(
      { 'function' => 'get_db_info',
        'version'  => 3,
        'params'   => \%params
      },
      $DEFBUFFER
    );
    $dllp->Close();
    #Prt( '-P', NowStr() . "   - return\n".HashDump(\%hydtables)."]\n" );
    
    #print Dumper(\%d);
    return %d;
  }
  
  
sub site_exists {
  my $c = shift;
  my $site = shift;
  my %params;
  $params{table_name} = 'site';
  $params{return_type} = 'array';
  $params{sitelist_filter} = $site;
  my $dllp = HydDllp->New();
  my @d = $dllp->get_db_info(
    { 'function' => 'get_db_info',
      'version'  => 3,
      'params'   => \%params
    },
    $DEFBUFFER
  );
  $dllp->Close();
  #Prt( '-P', NowStr() . "   - return\n".HashDump(\%hydtables)."]\n" );
  
  #print Dumper(\%d);
  if ( defined $d[0] ){
    return 1; 
  }
  else {
    return 0;
  }
}
    

  sub baseline_exists{
    my $c = shift;
    my $site = shift;
    my %params;
    $params{table_name} = 'areasmt';
    $params{return_type} = 'array';
    $params{sitelist_filter} = $site;
    my $dllp = HydDllp->New();
    my @d = $dllp->get_db_info(
      { 'function' => 'get_db_info',
        'version'  => 3,
        'params'   => \%params
      },
      $DEFBUFFER
    );
    $dllp->Close();
    #Prt( '-P', NowStr() . "   - return\n".HashDump(\%hydtables)."]\n" );
    #print Dumper(\%d);
    if ( defined $d[0] ){
      return 1; 
    }
    else {
      return 0;
    }
  }
  
  sub table_keys{
    my $c = shift;
    my $table = shift;
    my %params;
    my @complex_filter;
    my $key_no = 0;
    
    $params{complex_filter} = [
      { 
          'fieldname'=>'dbname',
          'operator'=>'EQ',
          'value'=>$table
      },
      {
          'combine'=>'AND',
          'fieldname'=>'keyfld',
          'operator'=>'EQ',
          'value'=>'true'
      }
    ];
    
    my @field_list = ('fldname');
    $params{table_name} = 'mastdict';
    $params{return_type} = 'hash';
    $params{field_list} = \@field_list;
    
    my $dllp = HydDllp->New();
    my %d = $dllp->get_db_info(
      { 'function' => 'get_db_info',
        'version'  => 3,
        'params'   => \%params
      },
      $DEFBUFFER
    );
    $dllp->Close();
    my %return_data;
    
    foreach my $table ( keys %d ){
      foreach my $field_key_no ( keys %{$d{$table}} ){
        foreach my $field ( keys %{$d{$table}{$field_key_no}} ){ 
          my $field_name = lc($field);
          $return_data{$field_name}++;
        }
      }
    }    
    return %return_data;
    #return %d;
  }
=skip  
  sub return_db_array {
    my $params_ref = shift;
    my %params = %{$params_ref};
    my $dllp = HydDllp->New();
    my @d = $dllp->get_db_info(
      { 'function' => 'get_db_info',
        'version'  => 3,
        'params'   => \%params
      },
      $DEFBUFFER
    );
    $dllp->Close();
    return @d;
  }
  
  sub return_db_hash {
    my $params_ref = shift;
    my %params = %{$params_ref};
    my $dllp = HydDllp->New();
    my %d = $dllp->get_db_info(
      { 'function' => 'get_db_info',
        'version'  => 3,
        'params'   => \%params
      },
      $DEFBUFFER
    );
    $dllp->Close();
    return %d;
  }

  
  
  sub company_exists{
    my $c = shift;
    my $table = shift;
    #my %ky = shift;
    my $kesy = shift;
    my %ky = %{$kesy};
    my %params;
    
    
    my $dllp = HydDllp->New();
    my @d = $dllp->get_db_info(
      { 'function' => 'get_db_info',
        'version'  => 3,
        'params'   => \%params
      },
      $DEFBUFFER
    );
    $dllp->Close();
    
    if ( defined $d[0] ){
      return 1; 
    }
    
    
    #return companyID
    
  }
=cut  
  sub key_exists{
    my $c = shift;
    my $table = shift;
    #my %ky = shift;
    my $kesy = shift;
    my %ky = %{$kesy};
    my %params;
    my @complex_filter;
    my $key_no = 0;
    
    foreach my $key ( keys %ky){
      $key_no++;
      my %complex;
      #print "key no [$key_no] fieldname [$key] value [$ky{$key}]\n";
      $complex{fieldname} = $key;
      $complex{operator} = 'EQ';
      $complex{value} = $ky{$key};
      
      if ($key_no > 1){
        $complex{combine} = 'AND';
      }
      
      push (@complex_filter, \%complex );
    }
    #print "Done. Getting complext filter for table [$table]. It looks like:\n";
    #foreach my $complex_no ( 0..$#complex_filter){
    #  print "  . complex #[$complex_no] = [$complex_filter[$complex_no]]\n";
    #}
    
    $params{table_name} = $table;
    $params{return_type} = 'array';
    $params{complex_filter} = \@complex_filter;
    
    my $dllp = HydDllp->New();
    my @d = $dllp->get_db_info(
      { 'function' => 'get_db_info',
        'version'  => 3,
        'params'   => \%params
      },
      $DEFBUFFER
    );
    $dllp->Close();
    #Prt( '-P', NowStr() . "   - return\n".HashDump(\%hydtables)."]\n" );
    #print Dumper(\%d);
    if ( defined $d[0] ){
      return 1; 
    }
    else {
      return 0;
    }
    
  }


  
  
  sub site_logger_config{
    my $c = shift;
    my $site = shift;
    my $dllp = HydDllp->New();
    my @instreg = $dllp->get_db_info(
      { 'function' => 'get_db_info',
        'version'  => 3,
        'params'   => {
          'table_name'  => 'instreg',
          'return_type' => 'array',
          'complex_filter' => [
            {
              'fieldname'=>'type',
              'operator'=>'EQ',
              'value'=>'LOGGER'
            },
            {
              'combine'=>'AND',
              'fieldname'=>'station',
              'operator'=>'EQ',
              'value'=>$site
            }
          ]
        }
      },
      $DEFBUFFER
    );
    
    my $model; 
    my $make;
    my $serial;
    
    if ( ! defined ( $instreg[0]{model} )){
      print "model not defined, for site [$site]";
      return 0;
    }
    else {
      $model = $instreg[0]{model};
    }
    
    if ( ! defined ( $instreg[0]{make} )){
      print "model not defined, for site [$site]";
      return 0;
    }
    else {
      $make = $instreg[0]{make};
      $serial = $instreg[0]{serial};
    }
        
    my @instmod = $dllp->get_db_info(
      { 'function' => 'get_db_info',
        'version'  => 3,
        'params'   => {
          'table_name'  => 'instmod',
          'return_type' => 'array',
          'complex_filter' => [
            {
              'fieldname'=>'model',
              'operator'=>'EQ',
              'value'=>$model
            },
            {
              'combine'=>'AND',
              'fieldname'=>'make',
              'operator'=>'EQ',
              'value'=>$make
            }
          ]
        }
      },
      $DEFBUFFER
    );
    
      my @instcal = $dllp->get_db_info(
      { 'function' => 'get_db_info',
        'version'  => 3,
        'params'   => {
          'table_name'  => 'instcal',
          'return_type' => 'array',
          'complex_filter' => [
            {
              'fieldname'=>'model',
              'operator'=>'EQ',
              'value'=>$model
            },
            {
              'combine'=>'AND',
              'fieldname'=>'make',
              'operator'=>'EQ',
              'value'=>$make
            },
            {
              'combine'=>'AND',
              'fieldname'=>'caltype',
              'operator'=>'EQ',
              'value'=>'calc'
            },
            {
              'combine'=>'AND',
              'fieldname'=>'serial',
              'operator'=>'EQ',
              'value'=>$serial
            }
          ]
        }
      },
      $DEFBUFFER
    );
    
    $dllp->Close();
    
    my %d;
    %{$d{instreg}}=%{$instreg[0]};
    %{$d{instmod}}=%{$instmod[0]};
    
    if ( ! defined $instcal[0] ){
      print "instcal not defined";
      return 0;
    }
    else{
      %{$d{instcal}}=%{$instcal[0]};
    } 
    
    
    my $notes = $d{instmod}{notes};
    my $comments = $d{instreg}{comment};
    my $calibrations = $d{instcal}{comment};
    #create a flattened hash of the memo sections in the return
    my %memos;
    %{$memos{notes}} = MemoHash($notes);
    %{$memos{comments}} = MemoHash($comments);
    %{$memos{calibrations}} = MemoHash($calibrations);
    
    foreach my $field (keys %memos){
      foreach my $section (keys %{$memos{$field}} ){
        %{$d{$section}} = %{$memos{$field}{$section}};
      }  
    }
    return %d;
  }

  
  sub process_row{
    my ($c,$inline,$stn,$config) = @_;
    my %config = %{$config};
    
    
    my $template            = $config{setup}{template};
    my $delimiter           = $config{setup}{delimiter};
    my $datetime_validation = $config{setup}{datetime_validation}//'';
    my $date_validation     = $config{setup}{date_validation}//'';
    my $time_validation     = $config{setup}{time_validation}//'';
    my $datetime_delimiter  = $config{setup}{datetime_delimiter}//''; 
    my $timedate_delimiter  = $config{setup}{timedate_delimiter}//''; 
    
    $delimiter              =~ s{"}{}g;
    $datetime_validation    =~ s{"}{}g;
    $date_validation        =~ s{"}{}g;
    $time_validation        =~ s{"}{}g;
    $datetime_delimiter     =~ s{"}{}g;
    $timedate_delimiter     =~ s{"}{}g;
    $inline                 =~ s{"}{}g;
    
    #print "template  $template\n";
    #print "file delim [$delimiter], datetime_delimiter [$datetime_delimiter], time_delimiter [$time_delimiter], date_delimiter [$date_delimiter]\n";
    #print "datetime_validation $datetime_validation\n";
    #print "date_validation $date_validation\n";
    #print "time_validation $time_validation\n";
    #print "c $c\n";
    my @template = split(/\,/,$template);
    my @data = split(qr/$delimiter/, $inline);
    my %return_data;
    my %variables;
    my ($standard_date,$standard_time);
    foreach my $temp_no ( 0.. $#template ) {
      #print "template element [ $template[$temp_no] ]\n";
      #print "data [ $data[$temp_no] ]\n";
      next if ( $template[$temp_no] eq 'ignore');
      given ($template[$temp_no]){
        when (/ignore/){
          next;
        }
        when (/datetimepm/){
          my ($date,$time,$pm) = split(qr/$datetime_delimiter/, $data[$temp_no]);
          #print "datetime [$template[$temp_no]] date.time [$date.$time]\n"; 
          $standard_date = NormaliseDate($date,$config);
          $standard_time = NormaliseTime($time,$config,$pm);
          #print "standard_time [$standard_time] standard_date [$standard_date]\n"; 
        }
        when (/datetime/){
          my ($date,$time) = split(qr/$datetime_delimiter/, $data[$temp_no]);
          #print "datetime [$template[$temp_no]] date.time [$date.$time]\n"; 
          $standard_date = NormaliseDate($date,$config);
          $standard_time = NormaliseTime($time,$config);
          #print "standard_time [$standard_time] standard_date [$standard_date]\n"; 
        }
        when (/timedatepm/){
          my ($time,$date,$pm) = split(qr/$timedate_delimiter/, $data[$temp_no]);
          #print "timedate [$date.$time]\n"; 
          $standard_date = NormaliseDate($date,$config);
          $standard_time = NormaliseTime($time,$config,$pm);
        }
        when (/timedate/){
          my ($time,$date) = split(qr/$timedate_delimiter/, $data[$temp_no]);
          #print "timedate [$date.$time]\n"; 
          $standard_date = NormaliseDate($date,$config);
          $standard_time = NormaliseTime($time,$config);
        }
        when (/date/){
          my $date = $data[$temp_no];
          #print "date [$date]\n"; 
          $standard_date = NormaliseDate($date,$config);
        }
        when (/time/){
          my $time = $data[$temp_no];
          #print "time [$time]\n"; 
          $standard_time = NormaliseTime($time,$config);
        }
        when (/timepm/){
          my ($time,$pm) = $data[$temp_no];
          #print "time [$time]\n"; 
          $standard_time = NormaliseTime($time,$config,$pm);
        }
        when (/^var_.*/){
          my $variable = $template[$temp_no];
          my $value = $data[$temp_no];
          $variable =~ s{^var_}{};
          $variables{$variable} = $value;
          #print "var [$variable] val [$value]\n"; 
        }
      }
    } 
    
    $stn = sprintf("%-15s",uc($stn));
    my $std_datetime = $standard_date.$standard_time; 
    #print "std datetime [$std_datetime]\n"; 
    
    my $formula = $config{setup}{formula};
    my $hz    = $variables{'118.00'};
    my $kpa   = $variables{'205.00'};
    my $temp  = $variables{'450.00'};
    my $calibrated_value;
    
    if ( !$kpa || $kpa == 0 ) {
      if ( defined $config{coefficients} ){
        my $c0 = $config{coefficients}{'c0'};
        my $c1 = $config{coefficients}{'c1'};
        my $c2 = $config{coefficients}{'c2'};
        my $c3 = $config{coefficients}{'c3'};
        my $c4 = $config{coefficients}{'c4'};
        my $c5 = $config{coefficients}{'c5'};
          
        $variables{'205.00'} = eval (lc($formula));  
        #print "calibrated_value [$calibrated_value]\n"; 
        #$kpa = ( !$kpa || $kpa == 0)? $calibrated_value: $kpa;
        #$variables{'205.00'} = $kpa;
      }
      elsif( defined $config{straight_line_calibration} ){
        
      }
      else{
      
      }
    }
    
    foreach my $var ( keys %variables ) {
      my $val = $variables{$var};
      $var = sprintf("%07.2f",$var);
      $val = sprintf("%011.5f",$val);
      my $key = $var.$std_datetime;
      $key =~ s{ }{}g;
      $return_data{$key} = qq{$stn $var 1 $std_datetime $val 051};
    }
    
    #RunJob("start /w hyextr $ts_site $datasource $variable $variable day 1 0 inst no 00:00_01/01/1980 00:00_02/01/1980 $templatefile $repfile /hide $hyconf",$repfile,\@progress);
#   my $calibrated_value = $C0 + ($C1*$hz) + ($C2*$temp) + ($C3 * $hz * $hz) + ($C4 * $hz * $temp) + ($C5 * $temp * $temp);
   #POLY Equation = f(X) = A*X^4 + B*X^3 + C*X^2 + D*X^1 + E*X^0
#   $kpa = ( !$kpa || $kpa == 0)? $calibrated_value: $kpa;
  

  
   #meter of head 1 kpa = 0.1019977334 moh
  # my $moh = $kpa * 0.1019977334;       
          
    #calculate Meters of Head if ther e are coefficients
    
    #my $stdhyd_time = sprintf("%04d%02d%02d%02d%02d%02d",$yyyy,$mm,$dd,$hh,$ii,$ee);
    
    #return $stdhyd_time ;
    # push (@{$da{$site}{'118.00'}}, $stdhyd_time.'_'.$hz);
    #  push (@{$da{$site}{'450.00'}}, $stdhyd_time.'_'.$temp);
    #  push (@{$da{$site}{'205.00'}}, $stdhyd_time.'_'.$kpa);
    #  push (@{$da{$site}{'119.00'}}, $stdhyd_time.'_'.$moh);
    # Prt(*DATA,qq{$stn $var 1 $date $val 51\n});
     
     # $stdhyd_time
     
     #push ( @return_rows,  
    # foreach variable 
    #my $stdhyd_time = sprintf("%04d%02d%02d% 02d%02d%02d",$yyyy,$mm,$dd,$hh,$ii,$ee);
    return %return_data;
  }
  
  
  sub NormaliseDate {
    my ($date,$config) = @_;
    my %config = %{$config};
    my $date_delimiter = $config{setup}{date_delimiter}//'';  
    my $date_validation = $config{setup}{date_validation}//'';  
    $date_delimiter  =~ s{"}{}g; 
    $date_validation =~ s{"}{}g; 
    
    #print "normalising date [$date] date_delimiter [$date_delimiter] date_validation [$date_validation]\n"; 
    
    my ($yyyy,$mm,$dd);
    my @dates = split(qr/$date_delimiter/, $date);
    my @validation_elements = split(qr/$date_delimiter/, $date_validation);
    #stdhyd date
    #yyyymmdd
    foreach my $element_no ( 0 .. $#validation_elements ){
      my $element = $validation_elements[$element_no];
      #print "validation element [$element]\n"; 
      given ($element){
        when (/yyyy/){
          if ( length( $dates[$element_no] ) == 2 ){
            $yyyy =  '20'.$dates[$element_no];
          }
          elsif( length( $dates[$element_no] ) == 4 ){
            $yyyy =  $dates[$element_no];
          }
        }
        when (/mm/){
          $mm = $dates[$element_no];
        }
        when (/dd/){
          $dd =  $dates[$element_no];
        }
      }
    }
    my $std_date = sprintf("%04d%02d%02d",$yyyy,$mm,$dd);
    #print "normalised date [$std_date]\n"; 
    return $std_date;
  }
  
  sub NormaliseTime {
    my ($time,$config,$pm) = @_;
    
    my %config = %{$config};
    my $time_delimiter = $config{setup}{time_delimiter}//'';  
    my $time_validation = $config{setup}{time_validation}//'';  
    $time_delimiter  =~ s{"}{}g; 
    $time_validation =~ s{"}{}g; 
    
    
    my @times = split(qr/$time_delimiter/, $time);  
    my @validation_elements = split(qr/$time_delimiter/, $time_validation);
    push (@validation_elements, lc($pm) ) if ( defined $pm );  
    #print "normalising time [$time] time_delimiter [$time_delimiter] time_validation [$time_validation], pm [$pm]\n"; 
    my ($hh,$ii,$ee);
    #stdhyd time
    #hhiiee
    foreach my $element_no ( 0 .. $#validation_elements ){
      my $element = $validation_elements[$element_no];
      
      #print "time validation element [$element]\n"; 
      
      given ($element){
        when (/hh/){
          $hh =  $times[$element_no];
        }
        when (/ii/){
          $ii =  $times[$element_no];
        }
        when (/ee/){
          $ee =  $times[$element_no];
        }
        when (/pm/){
          $hh = ( $hh < 12 )? $hh + 12:$hh;
        }
        when (/am/){
          $hh = ( $hh == 12)? 00:$hh;
        }
      }
    }  
    my $std_time = sprintf("%02d%02d%02d",$hh,$ii,$ee);  
    return $std_time;
  }
  
  
  sub MemoHash{
    my ($ininame) = shift;
    
    my ($sectkey, $value, %iniref, $keyword, $openname, $s, %sections);
    my $message = '';
    my $section = '##undef##';
    my @d = split(/\n/,$ininame);
    foreach my $s ( @d ){
      if ($s =~ m/^;/) {  #;comment - do nothing
      }
      elsif ($s =~ m/^\[(.*?)\]/) {  # [section name]
        $section  = lc($1);
        $message .= "The section [$section] appears more than once in $openname\n" if ($sections{$section}++);
      }
      elsif ($s =~ m/^\s*([a-zA-Z0-9].*?)\s*=(.*)/) { #keyword=value
        $keyword = lc($1);
        $value   = $2;
        #strip leading and trailing spaces off value
        $value =~ s/^\s+//;
        $value =~ s/\s+$//;
      }
      $iniref{$section}{$keyword} = $value;
    }
    return %iniref;
  }
=cut      
 }
  
  
=skip
 sub existing_data{
    my $d = shift;
    my $site = shift;
    my $table = shift;
    my $import = shift;
    my $existing = shift;
    
    print "Inputs: site[$site], table[$table] fields[\n$import]\n";
    foreach my $key ( keys %$existing){
      print "key [$key] value [$existing->{$key}]\n";
    }
    
    #hashwalk? Or normalise hash
    #
    foreach my $field ( keys %$import){
        print "Fields [$field] value [$import->{$field}]\n";
        
    }
 
   sub flatten_hash{
    my $data = @_;
      #foreach my $key ( keys %$data){
      #}
   #hash_walk{
      
    #}  
  }

  sub store_keys {
      my ($k, $v, $key_list,$table,$station) = @_;
      my $keystring = sprintf "%s", "@$key_list";
      $keystring =~ s{ }{}g;
      #if ( $k =~ m{^station}i ){
        #$data_temp{lc($table)}{lc($keystring)}{site}{$v}++;
        #$data_temp{lc($table)}{lc($keystring)}{lc($k)} = $v;
      #}
      #$data_temp{lc($table)}{lc($keystring)}{lc($k)} = $v;
      
  }  
  
  sub hash_walk {
      #call subroutine with: hash_walk(\%{$hydtable{$table}}, [], \&store_keys,$table);
      my ($hash, $key_list, $callback,$table) = @_;
      while (my ($k, $v) = each %$hash) {
          # Keep track of the hierarchy of keys, in case
          # our callback needs it.
          push @$key_list, $k;
          my $station = ( $k =~ m{^station}i )? $v : '';
          if (ref($v) eq 'HASH') {
              # Recursion.
             hash_walk($v, $key_list, $callback,$table);
          }
          else {
              # Otherwise, invoke our callback, passing it
              # the current key and value, along with the
              # full parentage of that key.
              
              my @k_list = @$key_list;
              #Prt('-P',"k list".HashDump(\@k_list)."]\n");
              pop @k_list;
              #Prt('-P',"k list popped ".HashDump(\@k_list)."]\n");
              #$callback->($k, $v, $key_list,$table);
              $callback->($k, $v, \@k_list,$table,$station);
          }
          pop @$key_list;
      }
  }  
 }
=cut    
    
    
      
=skip      
      #Prt('-P',"station [$station]\n");
      if ( defined $duplicats{$station} && ($table ne 'hydmeas' || $table ne 'hydrlmp')){
        Prt($prtdest_scr,NowStr()."         - [$station] is a duplicate, skipping\n");
        next;
      }
      else{
        Prt($prtdest_debug,NowStr()."         - writing key [$key]\n");
        $report{tables}{$table}{rowcount}++;
        $hydb{$table}->sethash(\%{$data{$table}{$key}});
        $hydb{$table}->write;
        $hydb{$table}->clear;
      } 
=cut
    no Moose;       # turn off Moose-specific scaffolding
 
    1;

}



=skip


{
package Hydstra;
use Moose;

has 'tables' => (
	is  => 'rw',
	isa => 'ArrayRef[Table]',
	);
}

{
package Hydstra::Tables;
use Moose;

extends 'Hydstra';

use DateTime;
my $dt    = DateTime->now; 
my $dmy   = $dt->dmy('/');   # 06/12/2002
my $hour   = $dt->hour;      # 0-23
my $minute = $dt->minute;     # 0-59

has 'aquifer' => (
	is  => 'rw',
  isa => 'Str',
  default  => $dmy,
	);
}

#isa => 'ArrayRef[Aquifer]',
#has 'hole' => ( isa => 'Hole', is => 'rw', required => 1 , default => sub { '1' });

#package Pet::Cat;
#has 'depthto' => ( isa => 'Depthto', is => 'rw', required => 0 );




{
package Hydstra::Tables::Aquifer;
use Moose;
use Moose::Util::TypeConstraints;
extends 'Hydstra::Tables';

subtype 'Station', as 'Str', where { length($_) <= 18 }, message {my $length = length($_); "Field must be less than 18 char, not [$length] characters.~"},;


has 'station' => (is => 'rw',	isa => 'Station', required => 1);


has 'hole' => (
	is       => 'rw',
	isa      => 'Int',
	default  => 1,
	required => 1,
);

has 'depthfrom' => (
	is       => 'rw',
	isa      => 'Int',
	default  => -999,
	required => 1,
);
  
}

=skip
my $poppy  = Pet::Bird->new( 
	name => 'Poppy' );

my $pet_shop = PetShop->new( 
	pets => [ $buster, $mimi, $addy, $poppy ] 
	);

my $pets = $pet_shop->pets;

=cut



	
=skip
%defaults = (
          aquifer  => {
                       hole => 1,
                      },
          casing  => {
                       hole => 1,
                       pipe => 1,
                      },
          gwhole   => {
                       hole => 1,
                      },
          history   => {
                       statdate   => $nowdat,
                       stattime   => $nowtim,
                       keyword    => 'NRMIMP'
                      },            
          lithdril   => {
                       hole => 1,
                       interpret => 'unknown',
                      },
          lithstra   => {
                       hole => 1,
                       interpret => 'unknown',
                       depthfrom => 0,
                       depthto => 0,
                      },
          gwpipe   => {
                       hole => 1,
                       pipe => 1,
                      },
          hydmeas  => {
                       hole => 1,
                       pipe => 1,
                       date => 19000101,
                       time => 0000,
                       quality => 50,
                       #variable => 110.00,         #only water levels are stored in HYMEAS, so variable is always 110
                      },
          hydrlmp  => {
                       hole => 1,
                       pipe => 1,
                       date => 19000101,
                       time => 0000,
                      },
          pumptest => {
                       hole => 1,
                       pipe => 1,
                       starttime => 0000,
                       timeoftest => 'NOT',
                      },
          gwtrace =>  {
                       hole => 1,
                       pipe => 1,
                       time => 0000,
                      },
          samples =>  {
                       bottle => 1,
                      },
          results =>  {
                       bottle => 1,
                       quality => 50,
                      },
          gwtracer => {
                       hole => 1,
                       pipe => 1,
                       time => 0000,
                       quality => 50,
                      },
          site     => {
                       active => 'T',
                       orgcode => 'QWR',
                       category5 => 'GWDB',
                      },
         );
=cut        


1;         