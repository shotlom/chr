@rem = ' Perl Script
@echo off
set mycd=homedir_%cd:\=\\%
perl -wS %0 %1 %2 %mycd% 
goto :EOF
';
undef @rem; 

use strict;
use warnings;

use IO::String;
use Env;
use Env qw(PATH HOME TERM);
use Cwd;
use List::Util qw(first max);

use FindBin qw($Bin); 
use local::lib '$Bin/lib';
use Git::Wrapper;



