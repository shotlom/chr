# 

Version 0.01

# SYNOPSIS

Run chr command line tools from the command line.

## Creating a README.md

To create a markdown readme file you need the Pod::Markdown module
At a standard DOS prompt do 

`pod2markdown my.pl > README.md`

- NOTE: Don't use the Git Powershell cmd as it has it's own version of Perl

## start

Start logging current project

_Usage_     
`chr start [project]`

## new

Create a new module from git template

_Usage_     
`chr new [project]`

## test

dmake test for module

_Usage_     
`chr test`

## test.verbose

verbose dmake test for module

_Usage_      
`chr test.verbose`

## validate.config

Validates json configs for importing

_Usage_      
`*** not implemented ***`

## gitonly

- Creates README.md
- Pushes to master branch of repo

_Usage_      
`chr gitonly "comment"`
